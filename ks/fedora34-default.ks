# Defualt f34 server install with added user (Do not use! Example only.)
# user: user, password: password
graphical

keyboard --xlayouts='us'
# System language
lang en_US.UTF-8

# Use CDROM installation media
cdrom

%packages
@^server-product-environment

%end

# Run the Setup Agent on first boot
firstboot --enable

# Generated using Blivet version 3.3.3
ignoredisk --only-use=vda
autopart
# Partition clearing information
clearpart --none --initlabel

# System timezone
timezone America/New_York --utc

# Root password
rootpw --lock
user --groups=wheel --name=user --password=$6$7voVZ.qBeRtqbdBJ$WTBUrnLscpJtllpIyMG/oVFjtQ5qyOPV4K646M7T/Nl8LgXOPHL3KA7sMYnffMIElBC8UlERapGEmOCN.yfkR1 --iscrypted --gecos="user"

reboot