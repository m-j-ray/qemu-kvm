


% users
snapraid

%packages

brtfs utils
mergerfs
snapper

%repos
mergerfs package repo

%post

# Install snapraid-brtfs (snapraid wrapper script)
git clone https://github.com/automorphism88/snapraid-btrfs.git
cd snapraid-btrfs
cp snapraid-btrfs /usr/local/bin
chmod +x /usr/local/bin/snapraid-btrfs

# snapraid-btrfs-runner (snapraid automation script)
git clone https://github.com/fmoledina/snapraid-btrfs-runner

