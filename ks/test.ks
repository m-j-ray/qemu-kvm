# commented items demo a micro install
#ignoredisk --only-use=vda
keyboard --vckeymap=us --xlayouts='us'
lang en_US.UTF-8
rootpw --iscrypted !!
timezone America/New_York --isUtc
bootloader
#bootloader --disabled
autopart
#autopart --type=plain --fstype=ext4
clearpart --none --initlabel
%packages --nocore
@^server-product-environment
%end
reboot