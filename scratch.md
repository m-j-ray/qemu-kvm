without Bridge network internet might not be available

# test fedora remote install

    sudo virt-install \
    --connect qemu:///system \
    --virt-type kvm \
    --name=fedora34-ks \
    --location=http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/os \
    --os-variant=fedora34 \
    --vcpus=2 \
    --memory=2048 \
    --disk size=10 \
    --extra-args="net.ifnames=0 inst.geoloc=0 inst.repo=http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/os console=tty0 console=ttyS0,115200n8 inst.sshd"

# test fedora remote install (KS) (kinda working)

    sudo virt-install \
    --connect qemu:///system \
    --virt-type kvm \
    --name=fedora34-ks \
    --location=http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/os \
    --os-variant=fedora34 \
    --vcpus=2 \
    --memory=2048 \
    --disk size=10 \
    --graphics none \
    --initrd-inject=ks/test.ks \
    --extra-args="inst.text net.ifnames=0 inst.geoloc=0 inst.ks=file:///test.ks inst.repo=http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/os console=tty0 console=ttyS0,115200n8 inst.sshd"

## Working!!!!!!


sudo virt-install \
--connect qemu:///system \
--virt-type kvm \
--name=fedora34-ks \
--location=http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/os \
--os-variant=fedora34 \
--vcpus=2 \
--memory=2048 \
--disk size=10 \
--graphics none \
--initrd-inject=ks/test.ks \
--extra-args="inst.text net.ifnames=0 inst.geoloc=0 inst.ks=file:///test.ks inst.repo=http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/os console=tty0 console=ttyS0,115200n8 inst.sshd"


# working centos remote install


sudo virt-install
--connect qemu:///system
--virt-type kvm
--name=centos-vm
--location=http://mirror.centos.org/centos/7/os/x86_64
--os-variant=centos7
--vcpus=2
--memory=2048
--disk size=10
--initrd-inject=ks/fedora34-default.ks
--extra-args="linux net.ifnames=0 inst.geoloc=0 inst.repo=http://mirror.centos.org/centos/7/os/x86_64 console=tty0 console=ttyS0,115200n8 inst.sshd"


