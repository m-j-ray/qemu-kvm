# qemu-kvm

### **Requirements**

Required packages

    libvirt
    qemu-kvm
    virt-install

Optional packages

    virt-viewer
    virt-manager


### **Create VM**

Create a simple VM using KVM.

    sudo virt-install \
    --connect qemu:///system \
    --virt-type kvm \
    --name=fedora34-vm \
    --location=/tmp/Fedora-Server-dvd-x86_64-34-1.2.iso \
    --os-variant=fedora34 \
    --vcpus=1 \
    --memory=1024 \
    --disk size=10

Create a VM from remote repo with automated install.

> Use for automated image builds 
>
> - Disable graphics during install: `--graphics none`
>
> - Prevent reboot and exit once install completes (when paired with `reboot` or `shutdown` kickstart command): `--noreboot`
>
> - Adds file to the root of the initrd: `--initrd-inject`
>
> Kernel command line arguments: `--extra-args`
> 
> - Location of a Kickstart file: `inst.ks`
>
> - Enables the sshd service during installation: `inst.sshd`

    sudo virt-install \
    --connect qemu:///system \
    --virt-type kvm \
    --name=fedora34-ks \
    --location=http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/os \
    --os-variant=fedora34 \
    --vcpus=1 \
    --memory=1024 \
    --disk size=10 \
    --graphics none \
    --noreboot \
    --initrd-inject=ks/fedora34-default.ks \
    --extra-args="inst.text net.ifnames=0 inst.geoloc=0 inst.ks=file:///fedora34-default.ks inst.repo=http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/os console=tty0 console=ttyS0,115200n8 inst.sshd"

Create a VM capable of nested virtualization with disk overcommit enabled.

> Pass direct access to host CPU `--cpu host`
>
> Do not check available space when creating disk `--check disk_size=off`
>

    sudo virt-install \
    --connect qemu:///system \
    --virt-type kvm \
    --name=fedora34-vm \
    --location=/tmp/Fedora-Server-dvd-x86_64-34-1.2.iso \
    --os-variant=fedora34 \
    --vcpus=2 \
    --cpu host \
    --memory=2048 \
    --disk size=30 \
    --check disk_size=off


### **Manage VM**

List all os variants

    osinfo-query os

List running VMs

    sudo virsh list

List all VMs

    sudo virsh list --all

Shutdown VM

    sudo virsh shutdown fedora34-vm

Force shutdown VM

    sudo virsh destroy fedora34-vm

Delete VM

> You cannot delete a VM that has snapshots

    sudo virsh undefine fedora34-vm

Delete VM and disks

    sudo virsh undefine fedora34-vm --remove-all-storage

List snapshots

    sudo virsh snapshot-list fedora34-vm

Take snapshot of VM

    sudo virsh snapshot-create fedora34-vm

Take snapshot of VM disk only (not VM state)

    sudo virsh snapshot-create fedora34-vm --disk-only

Take snapshot of VM with custom name and description

    sudo virsh snapshot-create-as fedora34-vm new-snapshot --description "test snapshot"

Revert VM to snapshot

    sudo virsh snapshot-revert fedora34-vm new-snapshot

Delete snapshot

    sudo virsh snapshot-delete fedora34-vm new-snapshot

### Networking

- Network bridge



