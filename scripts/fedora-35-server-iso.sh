#! /bin/bash
# f35 variant not yet available

sudo virt-install \
--connect qemu:///system \
--virt-type kvm \
--name=fedora35-server \
--location=/tmp/Fedora-Server-dvd-x86_64-35-1.2.iso \
--os-variant=fedora34 \
--vcpus=1 \
--memory=1024 \
--disk size=10