#! /bin/bash


sudo virt-install \
--connect qemu:///system \
--virt-type kvm \
--name=fedora35-server-gui \
--location=http://mirrors.kernel.org/fedora/releases/35/Server/x86_64/os \
--os-variant=fedora34 \
--vcpus=1 \
--memory=1024 \
--disk size=10 \
--extra-args="inst.repo=http://mirrors.kernel.org/fedora/releases/35/Server/x86_64/os"