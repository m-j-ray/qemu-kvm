#! /bin/bash
# f35 variant not yet available

# Fedora-Everything-netinst-x86_64-35-1.2.iso
# Fedora-Server-dvd-x86_64-35-1.2.iso
# Fedora-Server-netinst-x86_64-35-1.2.iso

# root
## ssh: connect to host 192.168.122.28 port 22: No route to host
# sudo virt-install \
# --connect qemu:///system \
# --virt-type kvm \
# --name=fedora34-server-custom \
# --location=/var/lib/libvirt/isos/Fedora-Server-dvd-x86_64-35-1.2.iso \
# --os-variant=fedora34 \
# --vcpus=2 \
# --memory=2048 \
# --disk size=30 \
# --graphics none \
# --noreboot \
# --initrd-inject=ks/fedora34-default.ks \
# --extra-args="inst.text net.ifnames=0 inst.geoloc=0 inst.ks=file:///fedora34-default.ks inst.repo=http://mirrors.kernel.org/fedora/releases/34/Server/x86_64/os console=tty0 console=ttyS0,115200n8 inst.sshd"

# root
sudo virt-install \
--connect qemu:///system \
--virt-type kvm \
--name=fedora34-server-custom \
--location=/var/lib/libvirt/isos/Fedora-Server-dvd-x86_64-35-1.2.iso \
--os-variant=fedora34 \
--vcpus=2 \
--memory=2048 \
--disk size=30 \
--noreboot \
--initrd-inject=ks/fedora34-default.ks \
--extra-args="inst.graphical net.ifnames=0 inst.geoloc=0 inst.ks=file:///fedora34-default.ks inst.sshd"
